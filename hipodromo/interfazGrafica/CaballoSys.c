#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "../InterfazUsuario/interfazGrafica.h"
#include "../InterfazUsuario/interfazUsuario.h"
#include "../InterfazUsuario/caballosIU.h"
#include "CaballoSys.h"
#include "../AccesoDatos/CaballoAD.h"


bool altaCaballoSys(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[], char msg[], char nomArchivo[])  //Transmite los datos del nuevo caballo desde la interfaz de usuario a la capa de acceso a datos.
{
    bool variable;
    variable = altaCaballoAD(nacionalidad,die,victorias,ganancias,edad,nombre,nomArchivo);

    gotoxy(16,31);
    printf("El caballo %s ha sido dado de alta correctamente en el archivo %s" , nombre , nomArchivo);
    if(variable ==false)
        return false;
    else
    {
        return true;
    }
}

bool generaInformeCaballos(char nomArchivo[])  //Genera un archivo de texto con los datos de todos los caballos registrados.
{
    long die;
    int victorias;
    char nombre[20];
    float ganancias;

    char nacionalidad[20];
    int edad;

    FILE * ptr2;
    FILE * ptr;
    int i;


    ptr=fopen("BaseDatos/caballosplus.txt","rt");
    ptr2=fopen("BaseDatos/infoCaballos.txt","wt");
    if (ptr2 == NULL)
    {
        muestraMensajeInfo("No se puede abrir el archivo infoCaballos.txt.\n");
        exit( 1 );

    }

    fprintf(ptr2,"-------------------------------------------------------\n");
    fprintf(ptr2,"Nacionalidad\tDie\tVictorias\tGanancias\tEdad\tNombre\n");
    fprintf(ptr2,"-------------------------------------------------------\n");


    while (fscanf(ptr,"%s\t",nacionalidad)==1)
    {
            fscanf(ptr,"%ld\t",&die);
            fscanf(ptr,"%d\t\t",&victorias);
            fscanf(ptr,"%f\t",&ganancias);
            fscanf(ptr,"%d\t",&edad);
            fscanf(ptr,"%s\n",nombre);
            i++;


        escribeCaballoTXT(nacionalidad,die,victorias,ganancias,edad,nombre,ptr2);
    }
    fprintf(ptr2,"\n------------------------------------------------------\n");


    fclose(ptr2);

    system("notepad BaseDatos/infoCaballos.txt");

    muestraMensajeInfo("                                                                     ");
    gotoxy(16,31);
    system("pause");
    inicInterfazUsuario();
    return true;
}


int cargaListaCaballosSys(char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[], char msgError[])  //Carga la lista de caballos

{
    int numCaballos;
    numCaballos=cargaListaCaballosAD(nacionalidad,die,victorias,ganancias,edad,nombre,nomArchivo);
    return numCaballos;
}

bool guardaListaCaballosSys(int numCaballos, char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[], char msgError[]) //Guarda la lista de caballos
{

    int i;

    i=guardaListaCaballosAD(numCaballos,nacionalidad,die,victorias,ganancias,edad,nombre,nomArchivo);

    if (i==0)
    {
        return true;
    }
    else
    {
        gotoxy(16,31);
        printf("%s",msgError);
        return false;
    }
    return numCaballos;
}
