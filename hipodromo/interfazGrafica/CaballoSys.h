#include <stdbool.h>
#define DIM_NOMBRE_CABALLO 20
#define NUM_CABALLO 50
#define DIM_NACIONALIDAD 20

bool altaCaballoSys(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[], char msg[], char nomArchivo[]);

bool generaInformeCaballos();

int cargaListaCaballosSys(char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[], char msgError[]);

bool guardaListaCaballosSys(int numCaballos, char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[], char msgError[]);
