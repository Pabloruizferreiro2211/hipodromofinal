#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "../InterfazUsuario/interfazGrafica.h"
#include "../InterfazUsuario/interfazUsuario.h"
#include "../InterfazGrafica/CaballoSys.h"
#define NUM_CABALLO 50
#include "../AccesoDatos/CaballoAD.h"
#include "caballosIU.h"


void altaCaballoIU() //Agrega los datos de un nuevo caballo introducido por teclado.
{
    char nombre[20];
    long die;
    int victorias;
    float ganancias;
    char nacionalidad[20];
    int edad;
    char msgError[50]="";

    inicInterfazUsuario();

    gotoxy(22,4);
    printf("Nuevo Caballo");

    muestraMensajeInfo("Introduzca la nacionalidad del caballo (Las 3 primeras letras del pais): ");
    scanf("%s",nacionalidad);

    gotoxy(1,8);
    printf("Nacionalidad: %s",nacionalidad);
    gotoxy(16,31);
    printf("                                                                                        ");

    muestraMensajeInfo("Introduzca DIE del caballo: ");
    scanf("%ld",&die);

    gotoxy(1,9);
    printf("DIE: %ld",die);
    gotoxy(16,31);
    printf("                                                                                        ");

    muestraMensajeInfo("Introduzca victorias del caballo: ");
    scanf("%d",&victorias);

    gotoxy(1,10);
    printf("Victorias: %d",victorias);
    gotoxy(16,31);
    printf("                                                                                        ");

    muestraMensajeInfo("Introduzca ganancias del caballo: ");
    scanf("%f",&ganancias);

    gotoxy(1,11);
    printf("Ganancias: %f",ganancias);
    gotoxy(16,31);
    printf("                                                                                        ");

    muestraMensajeInfo("Introduzca la edad del caballo: ");
    scanf("%d",&edad);

    gotoxy(1,12);
    printf("Edad: %d",edad);
    gotoxy(16,31);
    printf("                                                                                        ");

    muestraMensajeInfo("Introduzca el nombre: ");
    scanf("%s",nombre);

    gotoxy(1,13);
    printf("Nombre: %s",nombre);
    gotoxy(16,31);
    printf("                                                                                        ");


    altaCaballoSys(nacionalidad,die,victorias,ganancias,edad,nombre,msgError,"BaseDatos/caballosplus.txt");

    gotoxy(16,33);
    system("pause");

}



void muestraCaballo(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[]) //Muestra en pantalla los datos de un caballo.
{
    printf("%3s  %6ld %6d %10.2f %3d %20.20s",nacionalidad,die,victorias,ganancias,edad,nombre);
}


void gestionMenuCaballos()  //Gestiona la lista de caballos del hip�dromo.
{
    inicInterfazUsuario();
    int opcion1;
    do
    {
        opcion1=menuCaballos();
        switch(opcion1)
        {
        case 1:
            listadoCaballos("BaseDatos/caballosplus.txt");
            break;
        case 2:
            generaInformeCaballos();
            break;
        case 3:
            altaCaballoIU();
            break;
        case 4:
            actualizaCaballoIU();
            break;
        case 5:
            bajaCaballoIU();
            break;
        case 6:
            caballoMasGananciasIU();
            break;
        case 7:
            caballoMasVictoriasIU();
            break;
        case 8:
            jubilaCaballo();
            break;
        }

        inicInterfazUsuario();
    }
    while (opcion1!=0);

    inicInterfazUsuario();
}

void listadoCaballos(char nomArchivo[])  //Muestra un listado de los caballos registrados en pantalla.
{
    int victorias[50];
    char nombre[50][20];
    long die[50];
    float ganancias[50];
    char msgError[30]="";
    int edad[30];
    char nacionalidad[30][20];

    system("cls");
    inicInterfazUsuario();

    gotoxy(12,4);
    printf("L i s t a d o    c a b a l l o s               ");

    gotoxy(1,6);
    printf("NAC");

    gotoxy(8,6);
    printf("DIE");

    gotoxy(15,6);
    printf("VIC");

    gotoxy(24,6);
    printf("GAN");

    gotoxy(35,6);
    printf("EDA");

    gotoxy(50,6);
    printf("NOM");


    int numCaballos;

    numCaballos=cargaListaCaballosSys(nacionalidad,die,victorias,ganancias,edad,nombre,nomArchivo,msgError);

    int i,j=0;
    for (i=0; i<numCaballos; i++)
    {
        gotoxy(1,8+j);
        muestraCaballo(nacionalidad[i],die[i],victorias[i],ganancias[i],edad[i],nombre[i]);
        j++;
    }


    gotoxy(16,31);
    system("pause");

}

int menuCaballos(void)  //Muestra las opciones relacionadas con los caballos. Es el segundo men�.
{
    int opcion1;
    int num=10;
    gotoxy(14,4);
    printf("G e s t i o n   d e   c a b a l l o s");
    gotoxy(1,8);
    printf("1. Listado de caballos.");
    gotoxy(1,9);
    printf("2. Informe de caballos.");
    gotoxy(1,10);
    printf("3. Alta de un caballo.");
    gotoxy(1,11);
    printf("4. Actualizar caballo.");
    gotoxy(1,12);
    printf("5. Baja de un caballo.");
    gotoxy(1,13);
    printf("6. Caballo mas rentable.");
    gotoxy(1,14);
    printf("7. Caballo con mas victorias.");
    gotoxy(1,15);
    printf("8. Jubilar Caballo.");
    gotoxy(1,16);
    printf("0. Menu anterior.");


    opcion1=leerOpcionValida("Selecciona una opcion: ",num);
    return opcion1;

}



void caballoMasVictoriasIU () //Su objetivo es mostrar por pantalla el caballo con mas victorias
{
    int numCaballos;
    int i;
    int indice, victoriasTotales=0;
    int victorias[50];
    char nombre[50][20];
    long die[50];
    float ganancias[50];
    char msgError[30]=" ";
    char msg[]= {"El caballo con mas victorias es: "};
    char nacionalidad[30][20];
    int edad[30];



    numCaballos=cargaListaCaballosSys(nacionalidad,die,victorias,ganancias,edad,nombre,"BaseDatos/caballosplus.txt",msgError);


    system("cls");
    inicInterfazUsuario();

    gotoxy(6,4);
    printf("C a b a l l o   c o n   m a s   v i c t o r i a s");

    if (numCaballos<0)
    {
        muestraMensajeInfo("No se puede cargar la lista de caballos");
        system("pause");
        return;
    }


    for (i=0; i<numCaballos; i++)
    {
        if (victorias[i]>victoriasTotales)
        {
            victoriasTotales=victorias[i];
            indice=i;
        }
    }
    gotoxy(1,8);
    muestraCaballo(nacionalidad[indice],die[indice],victorias[indice],ganancias[indice],edad[indice],nombre[indice]);
    strcat(msg,nombre[indice]);
    muestraMensajeInfo(msg);
    gotoxy(16,33);
    system("pause");
}



void caballoMasGananciasIU () //Su objetivo es mostrar por pantalla el caballo con mas ganancias
{
    int numCaballos;
    int i;
    int indice, gananciasTotales=0;
    int victorias[50];
    char nombre[50][20];
    long die[50];
    float ganancias[50];
    char msg[]= {"El caballo con mas ganancias es: "};
    char nacionalidad[30][20];
    int edad[30];
    char msgError[30]=" ";


    numCaballos=cargaListaCaballosSys(nacionalidad,die,victorias,ganancias,edad,nombre,"BaseDatos/caballosplus.txt",msgError);

    system("cls");
    inicInterfazUsuario();

    gotoxy(6,4);
    printf("C a b a l l o   c o n   m a s    g a n a n c i a s");

    if (numCaballos<0)
    {
        muestraMensajeInfo("No se puede cargar la lista de caballos");
        system("pause");
        return ;
    }


    for (i=0; i<numCaballos; i++)
    {
        if (ganancias[i]>gananciasTotales)
        {

            gananciasTotales=ganancias[i];
            indice=i;
        }
    }

    gotoxy(1,8);
    muestraCaballo(nacionalidad[indice],die[indice],victorias[indice],ganancias[indice],edad[indice],nombre[indice]);

    strcat(msg,nombre[indice]);
    muestraMensajeInfo(msg);
    gotoxy(16,33);
    system("pause");
    inicInterfazUsuario();

}


void actualizaCaballoIU() //Su objetivo es actualizar las victorias y ganancias de un caballo cuando gana una carrera
{

    int victorias[50];
    int numCaballos;
    char nombre[50][20];
    char nombreganador[20];
    long die[50];
    float ganancias[50];
    float premio;
    char nacionalidad[30][20];
    int edad[30];
    char msgError[30]=" ";


    listadoCaballos("BaseDatos/caballosplus.txt");

    numCaballos=cargaListaCaballosSys(nacionalidad,die,victorias,ganancias,edad,nombre,"BaseDatos/caballosplus.txt",msgError);

    gotoxy(16,31);
    printf("                                                                                         ");
    muestraMensajeInfo("Introduce el nombre del ganador: ");
    gotoxy(50,31);
    scanf("%s",nombreganador);

    gotoxy(16,31);
    printf("                                                                                         ");
    muestraMensajeInfo("Introduce el numero de ganacias:");
    gotoxy(50,31);
    scanf("%f",&premio);

    int i,v=0;

    inicInterfazUsuario();

    for(i=0; i<numCaballos; i++)
    {
        if(strcmp(nombre[i],nombreganador)==0)
        {
            ganancias[i]=ganancias[i]+premio;
            victorias[i]=victorias[i]+1;
            v=v+1;
            guardaListaCaballosSys(numCaballos,nacionalidad,die,victorias,ganancias,edad,nombre,"BaseDatos/caballosplus.txt",msgError);
            listadoCaballos("BaseDatos/caballosplus.txt");
        }
    }

    if(v==0)
    {
        gotoxy(16,31);
        printf("                                                                                         ");
        muestraMensajeInfo("CABALLO NO ENCONTRADO");
    }
    else
    {
        gotoxy(16,31);
        printf("                                                                                         ");
        muestraMensajeInfo("CABALLO ACTUALIZADO CON EXITO");
    }

    gotoxy(16,31);
    printf("                                                                                            ");
    gotoxy(16,31);
    system("pause");

}


void bajaCaballoIU () //Su objetivo es dar de baja un caballo de la base de datos
{
    int victorias[50];
    char nombre[50][20];
    long die[50];
    long borradie;
    float ganancias[50];
    int numCaballos;
    char msgError[30]=" ";
    char nacionalidad[30][20];
    int edad[30];

    numCaballos=cargaListaCaballosSys(nacionalidad,die,victorias,ganancias,edad,nombre,"BaseDatos/caballosplus.txt",msgError);
    listadoCaballos("BaseDatos/caballosplus.txt");

    muestraMensajeInfo("Introduzca el DIE que desea borrar: ");
    gotoxy(52,31);
    scanf("%ld",&borradie);

    int i,j;
    int y=10;
    inicInterfazUsuario();

    for (i=0; i<numCaballos; i++)
    {
        if (die[i]==borradie)
        {
            for (j=i; j<numCaballos-1; j++)
            {
                strcpy(nombre[j],nombre[j+1]);
                die[j]=die[j+1];
                ganancias[j]=ganancias[j+1];
                victorias[j]=victorias[j+1];
            }

            numCaballos=numCaballos-1;
            guardaListaCaballosSys(numCaballos,nacionalidad,die,victorias,ganancias,edad,nombre,"BaseDatos/caballosplus.txt",msgError);
            muestraMensajeInfo("Se ha dado de baja el caballo de la base de datos");
            break;

        }

        else
        {
            gotoxy(4,4);
            printf("L i s t a d o   c a b a l l o s   a c t u a l i z a d o");
            gotoxy(1,y);
            muestraCaballo(nacionalidad[i],die[i],victorias[i],ganancias[i],edad[i],nombre[i]);
            y++;

        }

    }
    gotoxy(16,33);
    system("pause");
}

void muestraListaCaballos(int numCaballos, char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO]) //Esta funci�n es similar a la funci�n listadoCaballos(), con la diferencia de que recibe como par�metros un conjunto de vectores paralelos que contienen los datos sobre una lista de caballos.
{
    inicInterfazUsuario();

    gotoxy(12,4);
    printf("L i s t a d o    c a b a l l o s               ");

     gotoxy(1,6);
    printf("NAC");

    gotoxy(8,6);
    printf("DIE");

    gotoxy(15,6);
    printf("VIC");

    gotoxy(24,6);
    printf("GAN");

    gotoxy(35,6);
    printf("EDA");

    gotoxy(50,6);
    printf("NOM");


    int i,j=0;
    for (i=0; i<numCaballos; i++)
    {
        gotoxy(1,8+j);
        muestraCaballo(nacionalidad[i],die[i],victorias[i],ganancias[i],edad[i],nombre[i]);
        j++;
    }


    gotoxy(16,31);
    system("pause");
}

int eliminaRegistroCaballo(int x , int numCaballos , char nacionalidad [][60] , long int die[] , int victorias[] , float ganancias[] , int edad[] , char nombre[][60])
{
    for ( ; x< numCaballos ; x++)
    {
        strcpy(nacionalidad[x] , nacionalidad[x+1]);
        die[x] = die[x+1];
        victorias[x] = victorias[x+1];
        ganancias[x] = ganancias[x+1];
        edad[x] = edad[x+1];
        strcpy(nombre[x] , nombre[x+1]);
    }

    numCaballos--;
    return numCaballos;
}

void jubilaCaballo()
{

    long die[50];
    char nombre[50][50];
    char nacionalidad[50][50];
    char msgError[50] = "ERROR";
    int victorias[50];
    int edad[50];
    int edadju;
    float ganancias[50];
    int numCaballos , numCaballos2 , n = 0 , x;
    bool e;

    numCaballos = cargaListaCaballosSys(nacionalidad , die , victorias , ganancias , edad , nombre , "BaseDatos/caballosplus.txt" , msgError);
    muestraListaCaballos(numCaballos , nacionalidad , die , victorias , ganancias , edad , nombre);

    muestraMensajeInfo("Escriba la edad de jubilacion que quiera: ");
    scanf("%d" , &edadju);

    x = 0;

    while (x < numCaballos)
    {
        if (edad[x] >= edadju)
        {
            e = altaCaballoSys(nacionalidad[x] , die[x] , victorias[x] , ganancias[x] , edad[x] , nombre[x] , msgError , "BaseDatos/asiloCaballos.txt");

            if (e)
            {
                n++;
                muestraMensajeInfo(msgError);
                numCaballos2 = eliminaRegistroCaballo(x , numCaballos , nacionalidad , die , victorias , ganancias , edad , nombre) - (n-1);
            }
            else
            {
                x++;
            }
        }
        else
        {
            x++;
        }
    }

    muestraListaCaballos(numCaballos2 , nacionalidad , die , victorias , ganancias , edad , nombre);
    guardaListaCaballosSys(numCaballos2 , nacionalidad , die , victorias , ganancias , edad , nombre , "BaseDatos/asiloCaballos.txt" , msgError);
    listadoCaballos("BaseDatos/asiloCaballos.txt");

    return;

}
