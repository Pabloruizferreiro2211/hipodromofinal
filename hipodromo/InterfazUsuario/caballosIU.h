#define NUM_CABALLOS 50
#define DIM_NOMBRE_CABALLO 20
#define DIM_NACIONALIDAD 20
void altaCaballoIU();

void gestionMenuCaballos();

void listadoCaballos(char nomArchivo[]);

int menuCaballos(void);

void muestraCaballo(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[]);

void caballoMasVictoriasIU ();

void caballoMasGananciasIU();

void bajaCaballoIU();

void actualizaCaballoIU();

void muestraListaCaballos (int numCaballos, char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO]);

int eliminaRegistroCaballo(int x , int numCaballos , char nacionalidad [][60] , long int die[] , int victorias[] , float ganancias[] , int edad[] , char nombre[][60]);


void jubilaCaballo();
