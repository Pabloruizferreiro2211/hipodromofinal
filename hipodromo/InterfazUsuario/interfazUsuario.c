#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include "interfazGrafica.h"
#include "interfazUsuario.h"
#include "caballosIU.h"

void inicInterfazUsuario()  //Dibuja la interfaz �gr�fica� del hip�dromo.
{
    int x;
    int y;

    system("title Proyecto Integrador");
    system("mode con cols=125 lines=35");
    system("cls");
    SetColorText(71);

    gotoxy(0,0);
    printf("%c",201);
    for(x=1; x<=123; x++)
    {
        gotoxy(x,0);
        printf("%c",205);

    }
    for(x=1; x<=123; x++)
    {
        gotoxy(x,2);
        printf("%c",205);

    }
    gotoxy(124,0);
    printf("%c",187);
    gotoxy(0,2);
    printf("%c",200);
    gotoxy(124,2);
    printf("%c",188);
    gotoxy(0,1);
    printf("%c",186);
    gotoxy(124,1);
    printf("%c",186);
    gotoxy(0,3);
    printf("%c",218);
    gotoxy(124,3);
    printf("%c",191);
    gotoxy(0,5);
    printf("%c",195);
    gotoxy(0,4);
    printf("%c",179);
    gotoxy(0,7);
    printf("%c",195);
    for(x=1; x<=60; x++)
    {
        gotoxy(x,7);
        printf("%c",196);
    }

    gotoxy(0,6);
    printf("%c",179);

    for(x=1; x<=60; x++)
    {
        gotoxy(x,5);
        printf("%c",196);
    }
    gotoxy(124,4);
    printf("%c",179);
    gotoxy(124,5);
    printf("%c",180);
    gotoxy(124,6);
    printf("%c",179);
    gotoxy(124,7);
    printf("%c",180);
    gotoxy(0,28);
    printf("%c",192);
    gotoxy(124,28);
    printf("%c",217);
    for (y=8; y<=27; y++)
    {
        gotoxy(124,y);
        printf("%c",179);
    }
    for (y=8; y<=27; y++)
    {
        gotoxy(0,y);
        printf("%c",179);

    }
    gotoxy(61,3);
    printf("%c",191);
    for(x=1; x<=60; x++)
    {
        gotoxy(x,3);
        printf("%c",196);
    }
    gotoxy(62,3);
    printf("%c",218);
    for(x=63; x<=123; x++)
    {
        gotoxy(x,3);
        printf("%c",196);
    }
    gotoxy(61,28);
    printf("%c",217);
    for(x=1; x<=60; x++)
    {
        gotoxy(x,28);
        printf("%c",196);
    }
    gotoxy(62,28);
    printf("%c",192);
    for(x=63; x<=123; x++)
    {
        gotoxy(x,28);
        printf("%c",196);
    }
    gotoxy(61,4);
    printf("%c",179);
    gotoxy(61,5);
    printf("%c",180);
    gotoxy(61,6);
    printf("%c",179);
    gotoxy(61,7);
    printf("%c",180);
    for(y=8; y<=27; y++)
    {
        gotoxy(61,y);
        printf("%c",179);
    }

    gotoxy(62,4);
    printf("%c",179);
    gotoxy(62,5);
    printf("%c",195);
    for(x=63; x<=123; x++)
    {
        gotoxy(x,5);
        printf("%c",196);
    }
    gotoxy(62,6);
    printf("%c",179);
    gotoxy(62,7);
    printf("%c",195);
    for(x=63; x<=123; x++)
    {
        gotoxy(x,7);
        printf("%c",196);
    }
    for(y=8; y<=27; y++)
    {
        gotoxy(62,y);
        printf("%c",179);
    }
    gotoxy(15,34);
    printf("%c",192);
    gotoxy(15,30);
    printf("%c",218);
    for(y=31; y<=33; y++)
    {
        gotoxy(15,y);
        printf("%c",179);
    }
    gotoxy(109,30);
    printf("%c",191);
    gotoxy(109,34);
    printf("%c",217);
    for(y=31; y<=33; y++)
    {
        gotoxy(109,y);
        printf("%c",179);
    }
    for(x=16; x<=108; x++)
    {
        gotoxy(x,30);
        printf("%c",196);
    }
    for(x=16; x<=108; x++)
    {
        gotoxy(x,34);
        printf("%c",196);
    }

    gotoxy(15,32);
    printf("%c",195);
    gotoxy(109,32);
    printf("%c",180);
    for (x=16; x<=108; x++)
    {
        gotoxy(x,32);
        printf("%c",196);
    }
}

int menuPrincipal()  //Muestra las opciones generales del primer men� del hip�dromo.
{
    int opcion;
    int num=3;

    gotoxy(53,1);
    printf("H I P O D R O M O");
    gotoxy(18,4);
    printf("M e n u    p r i n c i p a l");
    gotoxy(1,8);
    printf("1. Gestion de caballos.");
    gotoxy(1,9);
    printf("2. Gestion de carreras.");
    gotoxy(1,10);
    printf("3. Inicio de carrera.");
    gotoxy(1,11);
    printf("0. Fin del programa.");

    opcion=leerOpcionValida("Selecciona una opcion: ",num);
    return opcion;
}

void gestionMenuPrincipal()  //Gestiona las opciones del men� principal.
{

    int opcion;
    do
    {
        opcion=menuPrincipal();
        switch(opcion)
        {
        case 1:
            gestionMenuCaballos();
            break;
        case 2:
            gotoxy(16,33);
            printf("Esta funcion no esta implementada");
            getch();
            break;
        case 3:
            gotoxy(16,33);
            printf("Esta funcion no esta implementada");
            getch();
            break;
        }
    }
    while (opcion!=0);

}

