#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "interfazGrafica.h"



void muestraMensajeInfo(char *msg)  //Muestra un mensaje en la ventana de mensajes.
{
    gotoxy(16,31);
    printf("                                                         ");
    gotoxy(16,31);
    printf(msg);
}

void gotoxy(int x, int y)   //Coloca el cursor en la columna x, fila y de la consola.
{
    COORD coord;

    coord.X = x;
    coord.Y = y;

    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);

    return;
}

void SetColorText(int color) //Determina los colores de tinta y fondo para el texto.
{
    system("color 7");
    return;
}




int leerOpcionValida (char *mensaje,char num)  //Solicita una opci�n al usuario y comprueba si es correcta.
{

    int opcion;

    muestraMensajeInfo(mensaje);
    scanf("%d",&opcion);
    return opcion;
}



