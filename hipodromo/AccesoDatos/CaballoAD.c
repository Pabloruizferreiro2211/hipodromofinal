#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "../InterfazUsuario/interfazGrafica.h"
#include "../InterfazUsuario/interfazUsuario.h"
#include "../InterfazGrafica/CaballoSys.h"
#include "../InterfazUsuario/caballosIU.h"
#include "CaballoAD.h"


int altaCaballoAD(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[], char nomArchivo[]) //Agrega los datos del nuevo caballo al archivo de texto caballos.txt
{


    FILE *ptr;
    ptr=fopen(nomArchivo,"at");

    if (ptr == NULL)
    {
        gotoxy(16,33);
        printf("No se puede abrir el archivo caballos.txt.\n");
        return -1;
    }
    else
    {
        fprintf(ptr,"%s\t%ld\t%d\t%f\t%d\t%s\n",nacionalidad,die,victorias,ganancias,edad,nombre);
    }

    fclose(ptr);
    return 0;

}


int cargaListaCaballosAD(char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[]) //Carga la lista de caballos
{

    int i=0;

    FILE *ptr;
    ptr=fopen(nomArchivo,"rt");

    if(ptr==NULL)
    {
        muestraMensajeInfo("No se puede abrir el archivo");
    }
    else
    {

        while (fscanf(ptr,"%s\t",nacionalidad[i])==1)
        {
            fscanf(ptr,"%ld\t",&die[i]);
            fscanf(ptr,"%d\t",&victorias[i]);
            fscanf(ptr,"%f\t",&ganancias[i]);
            fscanf(ptr,"%d\t",&edad[i]);
            fscanf(ptr,"%s\n",nombre[i]);
            i++;
        }
    }

    fclose(ptr);
    return i;

}

void escribeCaballoTXT(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[], FILE *ptr)  //Agrega los datos de un caballo al archivo infoCaballos.txt
{
        fprintf(ptr,"\n%s\t%ld\t%d\t\t%f\t%d\t%s\n",nacionalidad,die,victorias,ganancias,edad,nombre);
}


bool guardaListaCaballosAD(int numCaballos, char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[]) //Guarda la lista de caballos
{
    FILE *ptr;
    ptr=fopen(nomArchivo,"wt");

    if (ptr == NULL)
    {
        muestraMensajeInfo("No se puede abrir el archivo");
        return false;
    }

    else
    {
        int y,i;

        for (i=0; i<numCaballos; i++)
        {
            y++;
            gotoxy(3,y);
            escribeCaballoTXT(nacionalidad[i],die[i],victorias[i],ganancias[i],edad[i],nombre[i],ptr);
        }
        fclose(ptr);
        return true;
    }

}
