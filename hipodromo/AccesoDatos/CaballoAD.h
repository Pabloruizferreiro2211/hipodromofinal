#include <stdbool.h>
#define DIM_NOMBRE_CABALLO 20
#define NUM_CABALLO 50
#define DIM_NACIONALIDAD 20

int altaCaballoAD(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[], char nomArchivo[]);

int cargaListaCaballosAD(char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[]);

void escribeCaballoTXT(char nacionalidad[], long die, int victorias, float ganancias, int edad, char nombre[], FILE *ptr);

bool guardaListaCaballosAD(int numCaballos, char nacionalidad[][DIM_NACIONALIDAD], long die[], int victorias[], float ganancias[], int edad[], char nombre[][DIM_NOMBRE_CABALLO], char nomArchivo[]);

